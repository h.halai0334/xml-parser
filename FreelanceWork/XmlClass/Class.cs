﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FreelanceWork
{
    [XmlRoot(ElementName = "product_name")]
    public class ProductName
    {
        [XmlAttribute(AttributeName = "id")] public string Id { get; set; }
        [XmlAttribute(AttributeName = "name")] public string Name { get; set; }
        [XmlText] public string Text { get; set; }
    }

    [XmlRoot(ElementName = "author")]
    public class Author
    {
        [XmlElement(ElementName = "product_name")]
        public ProductName ProductName { get; set; }

        [XmlElement(ElementName = "product_version")]
        public string ProductVersion { get; set; }

        [XmlElement(ElementName = "product_revision")]
        public string ProductRevision { get; set; }
    }

    [XmlRoot(ElementName = "key")]
    public class Key
    {
        [XmlAttribute(AttributeName = "identifier")]
        public string Identifier { get; set; }
    }

    [XmlRoot(ElementName = "log_entry")]
    public class LogEntry
    {
        [XmlElement(ElementName = "author")] public Author Author { get; set; }
        [XmlElement(ElementName = "date")] public string Date { get; set; }

        [XmlElement(ElementName = "integrity")]
        public string Integrity { get; set; }

        [XmlElement(ElementName = "key")] public Key Key { get; set; }
    }

    [XmlRoot(ElementName = "document_log")]
    public class DocumentLog
    {
        [XmlElement(ElementName = "log_entry")]
        public List<LogEntry> LogEntry { get; set; }
    }

    [XmlRoot(ElementName = "entry")]
    public class Entry
    {
        [XmlAttribute(AttributeName = "name")] public string Name { get; set; }
        [XmlAttribute(AttributeName = "type")] public string Type { get; set; }
        [XmlText] public string Text { get; set; }
    }

    [XmlRoot(ElementName = "entries")]
    public class Entries
    {
        [XmlAttribute(AttributeName = "name")] public string Name { get; set; }
        [XmlElement(ElementName = "entry")] public List<Entry> Entry { get; set; }
        [XmlElement(ElementName = "entries")] public List<Entries> SubEntries { get; set; }
    }

    [XmlRoot(ElementName = "description")]
    public class Description
    {
        [XmlElement(ElementName = "document_id")]
        public string DocumentId { get; set; }

        [XmlElement(ElementName = "document_log")]
        public DocumentLog DocumentLog { get; set; }

        [XmlElement(ElementName = "entry")] public Entry Entry { get; set; }
    }

    [XmlRoot(ElementName = "shredder_erasure_report")]
    public class ShredderErasureReport
    {
        [XmlElement(ElementName = "entries")] public Entries Entries { get; set; }

        [XmlAttribute(AttributeName = "report")]
        public string Report { get; set; }
    }

    [XmlRoot(ElementName = "shredder_hardware_report")]
    public class ShredderHardwareReport
    {
        [XmlElement(ElementName = "entries")] public List<Entries> Entries { get; set; }
    }

    [XmlRoot(ElementName = "shredder_data")]
    public class ShredderData
    {
        [XmlElement(ElementName = "description")]
        public Description Description { get; set; }

        [XmlElement(ElementName = "shredder_erasure_report")]
        public ShredderErasureReport ShredderErasureReport { get; set; }

        [XmlElement(ElementName = "shredder_hardware_report")]
        public ShredderHardwareReport ShredderHardwareReport { get; set; }
    }

    [XmlRoot(ElementName = "user_data")]
    public class UserData
    {
        [XmlElement(ElementName = "entry")] public List<Entry> Entry { get; set; }
        [XmlAttribute(AttributeName = "name")] public string Name { get; set; }
    }

    [XmlRoot(ElementName = "report")]
    public class Report
    {
        [XmlElement(ElementName = "shredder_data")]
        public ShredderData ShredderData { get; set; }

        [XmlElement(ElementName = "user_data")]
        public UserData UserData { get; set; }
    }

    [XmlRoot(ElementName = "root")]
    public class Root
    {
        [XmlElement(ElementName = "report")] public Report Report { get; set; }
    }
}