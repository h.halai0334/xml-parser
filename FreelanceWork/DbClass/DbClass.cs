﻿using System;
using System.Collections.Generic;

namespace FreelanceWork.DbClass
{
    public class Base
    {
        public int Id { get; set; }
    }
    public class ReportTable : Base
    {
        public string DocumentId { get; set; }
        public string Verified { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string License { get; set; }
        public List<UserDataTable> UserDataTables { get; set; }
        public List<ReportLogTable> ReportLogTables { get; set; }
        public List<ReportErasureHardwareTable> ReportErasureHardwareTables { get; set; }
    }

    public class UserDataTable : Base
    {
        public int ReportTableId { get; set; }
        public ReportTable ReportTable { get; set; }
        public string UserName { get; set; }
        public string AssetId { get; set; }
        public string LotNo { get; set; }
    }

    public class ReportLogTable : Base
    {
        public int ReportTableId { get; set; }
        public ReportTable ReportTable { get; set; }
        public string ProductName { get; set; }
        public string ProductVersion { get; set; }
        public string ProductRevision { get; set; }
        public DateTime Date { get; set; }
        public string Key { get; set; }
        public string Integrity { get; set; }
    }

    public class ReportErasureHardwareTable : Base
    {
        public int ReportTableId { get; set; }
        public ReportTable ReportTable { get; set; }
        public HardwareBiosTable HardwareBiosTable { get; set; }
        public HardwareMotherBoardTable HardwareMotherBoardTable { get; set; }
        public HardwareSystemTable HardwareSystemTable { get; set; }
    }

    public class HardwareMotherBoardTable
    {
        public int ReportErasureHardwareTableId { get; set; }
        public ReportErasureHardwareTable ReportErasureHardwareTable { get; set; }
        public string Vendor { get; set; }
        public string ProductName { get; set; }
    }
    public class HardwareSystemTable
    {
        public int ReportErasureHardwareTableId { get; set; }
        public ReportErasureHardwareTable ReportErasureHardwareTable { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
    }

    public class HardwareBiosTable : Base
    {
        public int ReportErasureHardwareTableId { get; set; }
        public ReportErasureHardwareTable ReportErasureHardwareTable { get; set; }
        public string Vendor { get; set; }
        public string Version { get; set; }
        public uint RomSize { get; set; }
        public List<HardwareBiosTable> HardwareBiosTables { get; set; }
    }

    public class HardwareBiosFeatureTable : Base
    {
        public string Feature { get; set; }
    }
    public class ReportErasureLogTable : Base
    {
        public int ReportTableId { get; set; }
        public ReportTable ReportTable { get; set; }
        public uint ErasureId { get; set; }
        public string TimeStamp { get; set; }
        public uint CleanedSectors { get; set; }
        public uint TotalErrors { get; set; }
        public List<ReportErasureTargetTable> ReportTargets { get; set; }
        public List<ReportErasureStepTable> ReportSteps { get; set; }
        public List<ReportErasureExceptionTable> ReportErasureExceptions { get; set; }
    }

    public class ReportErasureTargetTable : Base
    {

        public int ReportErasureLogId { get; set; }
        public ReportErasureLogTable ReportErasureLogTable { get; set; }
        public uint TargetId { get; set; }
        public string Type { get; set; }
        public string Model { get; set; }
        public List<TargetRegionTable> Regions { get; set; }
    }

    public class ReportErasureExceptionTable
    {
        public int ReportErasureLogId { get; set; }
        public ReportErasureLogTable ReportErasureLogTable { get; set; }
        public string Exception { get; set; }
    }

    public class ReportErasureStepTable : Base
    {
        public int ReportErasureLogId { get; set; }
        public ReportErasureLogTable ReportErasureLogTable { get; set; }
        public uint Number { get; set; }
        public string Type { get; set; }
        public string Pattern { get; set; }
        public string ProcessedType { get; set; }
        public uint ProcessedSectors { get; set; }

    }
    public class TargetRegionTable : Base
    {
        public int ReportErasureTargetId { get; set; }
        public ReportErasureTargetTable ReportErasureTargetTable { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Capacity { get; set; }
    }
}