﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using XmlParser.Business.Interface;

namespace XmlParser.Business.Infrastructure
{
    public class HttpUtil : IHttpUtil
    {
        private readonly HttpClient _client;

        public HttpUtil(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> ExecuteLocalHost(string userName, string password, string fileName)
        {
            return Samples.XmlReportData.Data;
            using (_client)
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "http://localhost:8080/restservice/report/export/xml"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "text/xml");

                    var base64Authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{userName}:{password}"));
                    request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64Authorization}");

                    var multipartContent = new MultipartFormDataContent();
                    var file1 = new ByteArrayContent(File.ReadAllBytes(fileName));
                    file1.Headers.Add("Content-Type", "text/xml");
                    multipartContent.Add(file1, "xmlRequest", Path.GetFileName(fileName));
                    request.Content = multipartContent;
                    var response = await _client.SendAsync(request);
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }

    }
}