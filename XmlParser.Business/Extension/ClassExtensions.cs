﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace XmlParser.Business.Extension
{
    public static class ClassExtensions
    {
        
        public static bool IsPrimitive(this Type type)
        {
            if (type == typeof(String)) return true;
            return (type.IsValueType & type.IsPrimitive);
        }

        /// <summary>
        /// This functions copies all matching fields into the parameter child
        /// </summary>
        /// <param name="parent">Source</param>
        /// <param name="child">Target</param>
        /// <param name="ignoreProperty">Ignore this Property when copying data</param>
        /// <param name="isRecursive">Include Other Children</param>
        public static void CopyTo<T>(this T parent, T child,  string ignoreProperty = "", bool isRecursive = false)
        {
            var parentProperties = parent.GetType().GetProperties();
            var childProperties = child.GetType().GetProperties();
            foreach (var parentProperty in parentProperties)
            {
                foreach (var childProperty in childProperties)
                {
                    if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                    {
                        if (!string.IsNullOrWhiteSpace(ignoreProperty) && childProperty.Name == ignoreProperty)
                        {
                            break;
                        }

                        if (childProperty.PropertyType.IsPrimitive())
                        {
                            childProperty.SetValue(child, parentProperty.GetValue(parent));
                        }
                        else if(isRecursive)
                        {
                            IList parentList = (IList)parentProperty.GetValue(parent);
                            IList list = (IList)childProperty.GetValue(child);
                            for (int i = 0; i < list.Count; i++)
                            {
                                parentList[i].CopyTo(list[i],ignoreProperty,true);
                            }

                            if (list.Count < parentList.Count)
                            {
                                var startIndex = list.Count;
                                for (int i = startIndex; i < parentList.Count; i++)
                                {
                                    list.Add(parentList[i]);
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        public static void RemoveField<T>(this T parent, string name, bool isRecursive = false)
        {
            var parentProperties = parent.GetType().GetProperties();
            var returnClass = new ExpandoObject() as IDictionary<string, object>;
            for (int i = 0; i < parentProperties.Length; i++)
            {
                if (parentProperties[i].PropertyType.IsPrimitive())
                {
                        
                }
                if (parentProperties[i].Name != name)
                {
                    returnClass.Add(parentProperties[i].Name, parentProperties[i].GetValue(parent));
                }
            }
        }
    }
}