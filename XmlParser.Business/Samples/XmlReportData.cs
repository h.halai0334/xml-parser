﻿namespace XmlParser.Business.Samples
{
    public class XmlReportData
    {
        public static string Data = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<root>
	<report>
		<shredder_data>
			<description>
				<document_id>1c2eruii8394u39</document_id>
				<document_log>
					<log_entry>
						<author>
							<product_name id=""50"" name=""Shredder drive eraser"">Shredder 5</product_name>
							<product_version>1.1.1</product_version>
							<product_revision>1641(a12s6)</product_revision>
						</author>
						<date>2020-04-30T09:15:50+0000</date>
						<integrity>MCwCEfffssjdfkjds/12iheiwhr</integrity>
						<key identifier=""2""></key>
					</log_entry>
					<log_entry>
						<author>
							<product_name id=""51"" name=""Shredder console"">Shredder Console</product_name>
							<product_version>1.1.2</product_version>
							<product_revision>7a12s6e34</product_revision>
						</author>
						<date>2020-04-30T09:15:50+0000</date>
						<integrity>ADevbiuuewriewhfffssjdfkjds/12iheiwhr</integrity>
						<key identifier=""100""></key>
					</log_entry>
				</document_log>
				<entry name=""description_entries"">
					<entry name=""verified"" type=""string"">true</entry>
					<entries name=""company information"" >
						<entry name=""name"" type=""string"">XYZ</entry>
						<entry name=""address"" type=""string"">123 abcd efg</entry>
					</entries>
					<entry name=""license"" type=""string"">some company Edited</entry>
				</entry>
			</description>
			<shredder_erasure_report report="""">
				<entries name=""erasures"">
					<entry name=""erasure_id"" type=""uint"">1</entry>
					<entry name=""timestamp"" type=""string"">--</entry>
					<entry name=""cleaned_sectors"" type=""uint"">9</entry>
					<entry name=""total_errors"" type=""uint"">0</entry>
					<entries name=""target"">
						<entry name=""target_id"" type=""uint"">66</entry>
						<entry name=""type"" type=""string"">disk</entry>
						<entry name=""model"" type=""string"">SSD 850</entry>
						<entries name=""region"">
							<entry name=""type"" type=""string"">accessible</entry>
							<entry name=""capacity"" type=""uint"">5012321231233</entry>
							<entry name=""status"" type=""string"">Exist</entry>
						</entries>
						<entries name=""region"">
							<entry name=""type"" type=""string"">dco</entry>
							<entry name=""status"" type=""string"">Doesn't Exist</entry>
						</entries>
					</entries>
					<entries name=""erasure_details"">
						<entries name=""exception"">
							<entry name=""message"" type=""string"">Device is SSD</entry>
						</entries>
					</entries>
					<entries name=""steps"">
						<entries name=""step"">
							<entry name=""number"" type=""uint"">1</entry>
							<entry name=""type"" type=""string"">overwrite</entry>
							<entry name=""pattern"" type=""string"">random pattern</entry>
							<entries name=""processed"">
								<entry name=""type"" type=""string"">blocks</entry>
								<entry name=""n_sectors"" type=""uint"">970990809809</entry>
							</entries>
						</entries>
						<entries name=""step"">
							<entry name=""number"" type=""uint"">2</entry>
							<entry name=""type"" type=""string"">overwrite</entry>
							<entry name=""pattern"" type=""string"">random pattern</entry>
							<entries name=""processed"">
								<entry name=""type"" type=""string"">blocks</entry>
								<entry name=""n_sectors"" type=""uint"">8121231234</entry>
							</entries>
						</entries>
					</entries>
				</entries>
			</shredder_erasure_report>
			<shredder_hardware_report>
				<entries name=""system"">	
					<entry name=""manufacturer"" type=""string"">HP</entry>
					<entry name=""model"" type=""string"">HP EliteDesk</entry>
				</entries>
				<entries name=""bios"">
					<entry name=""vendor"" type=""string"">HP</entry>
					<entry name=""version"" type=""string"">L01 v02.31</entry>
					<entry name=""rom_size"" type=""uint"">1234456</entry>
					<entries name=""features"">
						<entry name=""feature"" type=""string"">PCI</entry>
						<entry name=""feature"" type=""string"">PNP</entry>
					</entries>
				</entries>

				<entries name=""motherboard"">
					<entry name=""vendor"" type=""string"">GSS</entry>
					<entry name=""product_name"" type=""string"">S27b</entry>
				</entries>
				<entries name=""processors"">
					<entry name=""total_cores"" type=""uint"">2</entry>
					<entry name=""total_cpus"" type=""uint"">1</entry>
					<entries name=""processor"">
						<entry name=""model"" type=""string"">Intel</entry>
						<entries name=""caches"">
							<entries name=""cache"">
								<entry name=""type"" type=""string"">Level 1 cache</entry>
								<entry name=""size"" type=""uint"">32123</entry>
							</entries>
							<entries name=""cache"">
								<entry name=""type"" type=""string"">Level 2 cache</entry>
								<entry name=""size"" type=""uint"">3242343</entry>
							</entries>
						</entries>
					</entries>
				</entries>
			</shredder_hardware_report>
		</shredder_data>
		<user_data name=""fields"">
			<entry name=""Asset ID"" type=""string"">12312344</entry>
			<entry name=""Lot #"" type=""string"">SMTR-1</entry>
			<entry name=""user"" type=""string"">khata</entry>
		</user_data>
	</report>

  <report>
    <shredder_data>
      <description>
        <document_id>2vc38345iuer21</document_id>
        <document_log>
          <log_entry>
            <author>
              <product_name id=""60"" name=""Shredder drive eraser"">Shredder 8</product_name>
              <product_version>1.1.2</product_version>
              <product_revision>1641(a12s6)</product_revision>
            </author>
            <date>2020-04-30T09:15:50+0000</date>
            <integrity>XPasadiwffssjdfkjds/12iheiwhr</integrity>
            <key identifier=""3""></key>
          </log_entry>
          <log_entry>
            <author>
              <product_name id=""61"" name=""Shredder console"">Shredder Console 8</product_name>
              <product_version>1.1.2</product_version>
              <product_revision>7a12s6e34</product_revision>
            </author>
            <date>2020-04-30T09:15:50+0000</date>
            <integrity>CDasdasfasuuewriewhfffssjdfkjds/12iheiwhr</integrity>
            <key identifier=""101""></key>
          </log_entry>
        </document_log>
        <entry name=""description_entries"">
          <entry name=""verified"" type=""string"">true</entry>
          <entries name=""company information"" >
            <entry name=""name"" type=""string"">XYZ</entry>
            <entry name=""address"" type=""string"">123 abcd efg</entry>
          </entries>
          <entry name=""license"" type=""string"">some company Edited</entry>
        </entry>
      </description>
      <shredder_erasure_report report="""">
        <entries name=""erasures"">
          <entry name=""erasure_id"" type=""uint"">1</entry>
          <entry name=""timestamp"" type=""string"">--</entry>
          <entry name=""cleaned_sectors"" type=""uint"">9</entry>
          <entry name=""total_errors"" type=""uint"">0</entry>
          <entries name=""target"">
            <entry name=""target_id"" type=""uint"">66</entry>
            <entry name=""type"" type=""string"">disk</entry>
            <entry name=""model"" type=""string"">SSD 850</entry>
            <entries name=""region"">
              <entry name=""type"" type=""string"">accessible</entry>
              <entry name=""capacity"" type=""uint"">5012321231233</entry>
              <entry name=""status"" type=""string"">Exist</entry>
            </entries>
            <entries name=""region"">
              <entry name=""type"" type=""string"">dco</entry>
              <entry name=""status"" type=""string"">Doesn't Exist</entry>
            </entries>
          </entries>
          <entries name=""erasure_details"">
            <entries name=""exception"">
              <entry name=""message"" type=""string"">Device is SSD</entry>
            </entries>
          </entries>
          <entries name=""steps"">
            <entries name=""step"">
              <entry name=""number"" type=""uint"">1</entry>
              <entry name=""type"" type=""string"">overwrite</entry>
              <entry name=""pattern"" type=""string"">random pattern</entry>
              <entries name=""processed"">
                <entry name=""type"" type=""string"">blocks</entry>
                <entry name=""n_sectors"" type=""uint"">970990809809</entry>
              </entries>
            </entries>
            <entries name=""step"">
              <entry name=""number"" type=""uint"">2</entry>
              <entry name=""type"" type=""string"">overwrite</entry>
              <entry name=""pattern"" type=""string"">random pattern</entry>
              <entries name=""processed"">
                <entry name=""type"" type=""string"">blocks</entry>
                <entry name=""n_sectors"" type=""uint"">8121231234</entry>
              </entries>
            </entries>
          </entries>
        </entries>
      </shredder_erasure_report>
      <shredder_hardware_report>
        <entries name=""system"">
          <entry name=""manufacturer"" type=""string"">HP</entry>
          <entry name=""model"" type=""string"">HP EliteDesk</entry>
        </entries>
        <entries name=""bios"">
          <entry name=""vendor"" type=""string"">HP</entry>
          <entry name=""version"" type=""string"">L01 v02.31</entry>
          <entry name=""rom_size"" type=""uint"">1234456</entry>
          <entries name=""features"">
            <entry name=""feature"" type=""string"">PCI</entry>
            <entry name=""feature"" type=""string"">PNP</entry>
          </entries>
        </entries>

        <entries name=""motherboard"">
          <entry name=""vendor"" type=""string"">GSS</entry>
          <entry name=""product_name"" type=""string"">S27b</entry>
        </entries>
        <entries name=""processors"">
          <entry name=""total_cores"" type=""uint"">2</entry>
          <entry name=""total_cpus"" type=""uint"">1</entry>
          <entries name=""processor"">
            <entry name=""model"" type=""string"">Intel</entry>
            <entries name=""caches"">
              <entries name=""cache"">
                <entry name=""type"" type=""string"">Level 1 cache</entry>
                <entry name=""size"" type=""uint"">32123</entry>
              </entries>
              <entries name=""cache"">
                <entry name=""type"" type=""string"">Level 2 cache</entry>
                <entry name=""size"" type=""uint"">3242343</entry>
              </entries>
            </entries>
          </entries>
        </entries>
      </shredder_hardware_report>
    </shredder_data>
    <user_data name=""fields"">
      <entry name=""Asset ID"" type=""string"">4387568</entry>
      <entry name=""Lot #"" type=""string"">SMTR-2</entry>
      <entry name=""user"" type=""string"">pam</entry>
    </user_data>
  </report>
</root>";
    }
}