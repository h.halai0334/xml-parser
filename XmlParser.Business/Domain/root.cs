﻿using System.Collections.Generic;

namespace XmlParser.Business.Domain
{
    public class root : Base
    {
        public List<root_report> root_reportList { get; set; }
    }

    public class root_report : Base
    {
        public List<shredder_data> shredder_dataList { get; set; }
        public List<fields> fieldsList { get; set; }
    }

    public class shredder_data : Base
    {
        public List<shredder_data_description> shredder_data_descriptionList { get; set; }
        public List<shredder_data_shredder_erasure_report> shredder_data_shredder_erasure_reportList { get; set; }
        public List<shredder_data_shredder_hardware_report> shredder_data_shredder_hardware_reportList { get; set; }
    }

    public class shredder_data_description : Base
    {
        public string document_id { get; set; }
        public List<shredder_data_description_document_log> shredder_data_description_document_logList { get; set; }

        public List<shredder_data_description_description_entries> shredder_data_description_description_entriesList
        {
            get;
            set;
        }
    }

    public class shredder_data_description_document_log : Base
    {
        public List<shredder_data_description_document_log_log_entry>
            shredder_data_description_document_log_log_entryList { get; set; }
    }

    public class shredder_data_description_document_log_log_entry : Base
    {
        public List<shredder_data_description_document_log_log_entry_author>
            shredder_data_description_document_log_log_entry_authorList { get; set; }

        public string date { get; set; }
        public string integrity { get; set; }
        public string key { get; set; }
        public string key_identifier { get; set; }
    }

    public class shredder_data_description_document_log_log_entry_author : Base
    {
        public string product_name { get; set; }
        public string product_name_id { get; set; }
        public string product_name_name { get; set; }
        public string product_version { get; set; }
        public string product_revision { get; set; }
    }

    public class shredder_data_description_description_entries : Base
    {
        public string verified { get; set; }

        public List<shredder_data_description_description_entries_company_information>
            shredder_data_description_description_entries_company_informationList { get; set; }

        public string license { get; set; }
    }

    public class shredder_data_description_description_entries_company_information : Base
    {
        public string name { get; set; }
        public string address { get; set; }
    }

    public class shredder_data_shredder_erasure_report : Base
    {
        public List<shredder_data_shredder_erasure_report_erasures> shredder_data_shredder_erasure_report_erasuresList
        {
            get;
            set;
        }
    }

    public class shredder_data_shredder_erasure_report_erasures : Base
    {
        public string erasure_id { get; set; }
        public string timestamp { get; set; }
        public string cleaned_sectors { get; set; }
        public string total_errors { get; set; }

        public List<shredder_data_shredder_erasure_report_erasures_target>
            shredder_data_shredder_erasure_report_erasures_targetList { get; set; }

        public List<shredder_data_shredder_erasure_report_erasures_erasure_details>
            shredder_data_shredder_erasure_report_erasures_erasure_detailsList { get; set; }

        public List<shredder_data_shredder_erasure_report_erasures_steps>
            shredder_data_shredder_erasure_report_erasures_stepsList { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_target : Base
    {
        public string target_id { get; set; }
        public string type { get; set; }
        public string model { get; set; }

        public List<shredder_data_shredder_erasure_report_erasures_target_region>
            shredder_data_shredder_erasure_report_erasures_target_regionList { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_target_region : Base
    {
        public string type { get; set; }
        public string capacity { get; set; }
        public string status { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_erasure_details : Base
    {
        public List<shredder_data_shredder_erasure_report_erasures_erasure_details_exception>
            shredder_data_shredder_erasure_report_erasures_erasure_details_exceptionList { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_erasure_details_exception : Base
    {
        public string message { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_steps : Base
    {
        public List<shredder_data_shredder_erasure_report_erasures_steps_step>
            shredder_data_shredder_erasure_report_erasures_steps_stepList { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_steps_step : Base
    {
        public string number { get; set; }
        public string type { get; set; }
        public string pattern { get; set; }

        public List<shredder_data_shredder_erasure_report_erasures_steps_step_processed>
            shredder_data_shredder_erasure_report_erasures_steps_step_processedList { get; set; }
    }

    public class shredder_data_shredder_erasure_report_erasures_steps_step_processed : Base
    {
        public string type { get; set; }
        public string n_sectors { get; set; }
    }

    public class shredder_data_shredder_hardware_report : Base
    {
        public List<shredder_data_shredder_hardware_report_system> shredder_data_shredder_hardware_report_systemList
        {
            get;
            set;
        }

        public List<shredder_data_shredder_hardware_report_bios> shredder_data_shredder_hardware_report_biosList
        {
            get;
            set;
        }

        public List<shredder_data_shredder_hardware_report_motherboard>
            shredder_data_shredder_hardware_report_motherboardList { get; set; }

        public List<shredder_data_shredder_hardware_report_processors>
            shredder_data_shredder_hardware_report_processorsList { get; set; }
    }

    public class shredder_data_shredder_hardware_report_system : Base
    {
        public string manufacturer { get; set; }
        public string model { get; set; }
    }

    public class shredder_data_shredder_hardware_report_bios : Base
    {
        public string vendor { get; set; }
        public string version { get; set; }
        public string rom_size { get; set; }

        public List<shredder_data_shredder_hardware_report_bios_features>
            shredder_data_shredder_hardware_report_bios_featuresList { get; set; }
    }

    public class shredder_data_shredder_hardware_report_bios_features : Base
    {
        public string feature { get; set; }
    }

    public class shredder_data_shredder_hardware_report_motherboard : Base
    {
        public string vendor { get; set; }
        public string product_name { get; set; }
    }

    public class shredder_data_shredder_hardware_report_processors : Base
    {
        public string total_cores { get; set; }
        public string total_cpus { get; set; }

        public List<shredder_data_shredder_hardware_report_processors_processor>
            shredder_data_shredder_hardware_report_processors_processorList { get; set; }
    }

    public class shredder_data_shredder_hardware_report_processors_processor : Base
    {
        public string model { get; set; }

        public List<shredder_data_shredder_hardware_report_processors_processor_caches>
            shredder_data_shredder_hardware_report_processors_processor_cachesList { get; set; }
    }

    public class shredder_data_shredder_hardware_report_processors_processor_caches : Base
    {
        public List<shredder_data_shredder_hardware_report_processors_processor_caches_cache>
            shredder_data_shredder_hardware_report_processors_processor_caches_cacheList { get; set; }
    }

    public class shredder_data_shredder_hardware_report_processors_processor_caches_cache : Base
    {
        public string type { get; set; }
        public string size { get; set; }
    }

    public class fields : Base
    {
        public string asset_id { get; set; }
        public string lot_no { get; set; }
        public string user { get; set; }
    }
}