﻿using System.Threading.Tasks;

namespace XmlParser.Business.Interface
{
    public interface IHttpUtil
    {
        Task<string> ExecuteLocalHost(string userName, string password, string fileName);
    }
}