﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using XmlParser.Business.Domain;

namespace XmlParser.Business.Interface
{
    public interface IXmlParser
    {
        (Dictionary<string, HashSet<string>>, string) GenerateClassFromXml(string xml);
        (root, string) ParseXml(string xml);
    }
}