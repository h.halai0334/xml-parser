﻿
using System;
using System.Reflection;
using AzureFunctions.Extensions.Swashbuckle;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using XmlParser.Business.Context;
using XmlParser.Business.Infrastructure;
using XmlParser.Business.Interface;
using XmlParser.FunctionApp;

[assembly: FunctionsStartup(typeof(Startup))]
namespace XmlParser.FunctionApp
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddScoped(p =>
            {
                var connectionString = Environment.GetEnvironmentVariable("ConnectionString");
                return new ApplicationDbContext(connectionString);
            });
            builder.Services.AddScoped<IXmlParser, Business.Parser.XmlParser>();
            builder.Services.AddScoped<IHttpUtil, HttpUtil>();
            builder.AddSwashBuckle(Assembly.GetExecutingAssembly(), opts =>
            {
                opts.XmlPath = "XmlParser.FunctionApp.xml";
            });

        }
    }
    
}