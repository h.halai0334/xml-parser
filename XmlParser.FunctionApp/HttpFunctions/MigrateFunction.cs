﻿using System.Net.Http;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using XmlParser.Business.Context;
using XmlParser.Business.Interface;

namespace XmlParser.FunctionApp.HttpFunctions
{
    public class MigrateFunction
    {
        private readonly IXmlParser _xmlParser;
        private readonly IHttpUtil _httpUtil;
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<MigrateFunction> _logger;
        public MigrateFunction(ApplicationDbContext dbContext, ILogger<MigrateFunction> logger, IHttpUtil httpUtil, IXmlParser xmlParser)
        {
            _dbContext = dbContext;
            _logger = logger;
            _httpUtil = httpUtil;
            _xmlParser = xmlParser;
        }

        [FunctionName("Migrate")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v1/migrate")]
            HttpRequestMessage req)
        {
            try
            {
                
                _dbContext.DeleteAllTables();
                _dbContext.CreateAllTables();
                
                // Enter you hardcoded username, password and fileName
                var xml = await _httpUtil.ExecuteLocalHost("","","");
                (Business.Domain.root result, string exception) = _xmlParser.ParseXml(xml);
                if (!string.IsNullOrWhiteSpace(exception))
                {
                    return new JsonResult(new
                    {
                        message = exception,
                        code = 500,
                        success = false
                    });
                }
                await _dbContext.AddRangeAsync(result);
                await _dbContext.SaveChangesAsync();
                return new JsonResult(new
                {
                    code = 200,
                    sucess = true,
                    message = "Saved into the database successfully"
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message + "\n" + e.InnerException?.Message);
                return new JsonResult(new
                {
                    message = e.Message,
                    code = 500,
                    success = false
                });
            }
        }

        
    }
}