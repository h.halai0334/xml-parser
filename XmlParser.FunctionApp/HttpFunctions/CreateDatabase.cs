﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using XmlParser.Business.Context;
using XmlParser.FunctionApp.Models;

namespace XmlParser.FunctionApp.HttpFunctions
{
    public class CreateDatabase
    {
        private readonly ApplicationDbContext _dbContext;

        public CreateDatabase(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Create Database
        /// </summary>
        /// <param name="req"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [FunctionName("CreateDatabase")]
        public IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "database")]
            HttpRequest req, ILogger log)
        {
            try
            {
                _dbContext.CreateAllTables();
                return new OkObjectResult(new SuccessModel());
            }
            catch (Exception e)
            {
                return new OkObjectResult(new ErrorModel(e.Message));
            }   
        }
    }
}