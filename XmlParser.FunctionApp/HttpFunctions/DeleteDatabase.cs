﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using XmlParser.Business.Context;
using XmlParser.FunctionApp.Models;

namespace XmlParser.FunctionApp.HttpFunctions
{
    
    public class DeleteDatabase
    {
        private readonly ApplicationDbContext _dbContext;

        public DeleteDatabase(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        

        /// <summary>
        /// Delete Database
        /// </summary>
        [FunctionName("DeleteDatabase")]
        public IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "database")]
            HttpRequest req, ILogger log)
        {
            try
            {
                _dbContext.DeleteAllTables();
                return new OkObjectResult(new SuccessModel());
            }
            catch (Exception e)
            {
                return new OkObjectResult(new ErrorModel(e.Message));
            }
        }
    }
}