﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using XmlParser.Business.Interface;
using XmlParser.FunctionApp.Models;

namespace XmlParser.FunctionApp.HttpFunctions
{
    public class GenerateClass
    {
        private readonly IXmlParser _xmlParser;
        private readonly IHttpUtil _httpUtil;
        public GenerateClass(IXmlParser xmlParser, IHttpUtil httpUtil)
        {
            _xmlParser = xmlParser;
            _httpUtil = httpUtil;
        }

        /// <summary>
        /// Converts XML to Classes
        /// </summary>
        [FunctionName("GenerateClass")]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]
            HttpRequest req, ILogger log)
        {
            var xml = await _httpUtil.ExecuteLocalHost("", "", "");
            (var result, string exception) = _xmlParser.GenerateClassFromXml(xml);
            string classData = "";
            foreach (KeyValuePair<string,HashSet<string>> classItem in result)
            {
                classData += "public class " + classItem.Key + ": Base {\n";
                foreach (var property in classItem.Value)
                {
                    classData += property + "\n";
                }
                classData += "}\n";
            }
            return new OkObjectResult(new SuccessDataModel(new
            {
                text = classData
            }));

        }
    }
}