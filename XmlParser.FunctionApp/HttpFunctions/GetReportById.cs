﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using XmlParser.Business.Context;
using XmlParser.Business.Domain;
using XmlParser.Business.Extension;
using XmlParser.FunctionApp.Models;

namespace XmlParser.FunctionApp.HttpFunctions
{
    public class GetReportById
    {
        private readonly ApplicationDbContext _dbContext;

        public GetReportById(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [FunctionName("GetReportById")]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "report/{id}")]
            HttpRequest req,
            string id,
            ILogger log)
        {
            var dbResult = _dbContext.Roots.Where(p => p.root_reportList.Any(pr =>
                    pr.shredder_dataList.Any(prr =>
                        prr.shredder_data_descriptionList.Any(prrr =>
                            prrr.document_id == id))))
                .Include(_dbContext.GetIncludePaths(typeof(root))).FirstOrDefault();

            if (dbResult != null)
            {
                var targetData = dbResult.root_reportList.First(pr => pr.shredder_dataList.Any(prr =>
                    prr.shredder_data_descriptionList.Any(prrr =>
                        prrr.document_id == id)));
                return new OkObjectResult(new SuccessDataModel(targetData));
            }
            return new OkObjectResult(new ErrorModel("Report Not Found"));
        }
    }
}