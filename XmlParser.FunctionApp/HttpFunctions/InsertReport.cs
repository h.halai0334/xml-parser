﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using XmlParser.Business.Context;
using XmlParser.Business.Domain;
using XmlParser.Business.Extension;
using XmlParser.Business.Interface;
using XmlParser.FunctionApp.Models;

namespace XmlParser.FunctionApp.HttpFunctions
{
    public class InsertReport
    {
        private readonly IXmlParser _xmlParser;
        private readonly IHttpUtil _httpUtil;
        private readonly ApplicationDbContext _dbContext;

        public InsertReport(IXmlParser xmlParser, IHttpUtil httpUtil, ApplicationDbContext dbContext)
        {
            _xmlParser = xmlParser;
            _httpUtil = httpUtil;
            _dbContext = dbContext;
        }

        /// <summary>
        /// Insert Report
        /// </summary>
        [FunctionName("InsertReport")]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "report")]
            HttpRequest req, ILogger log)
        {
            try
            {
                var xml = await _httpUtil.ExecuteLocalHost("", "", "");
                (root result, string exception) = _xmlParser.ParseXml(xml);
                if (!string.IsNullOrWhiteSpace(exception))
                {
                    return new OkObjectResult(new ErrorModel(exception));
                }

                for (var index = 0; index < result.root_reportList.Count; index++)
                {
                    var report = result.root_reportList[index];
                    for (var i = 0; i < report.shredder_dataList.Count; i++)
                    {
                        var shredderData = report.shredder_dataList[i];
                        for (var index1 = 0; index1 < shredderData.shredder_data_descriptionList.Count; index1++)
                        {
                            var description = shredderData.shredder_data_descriptionList[index1];
                            var dbResult = _dbContext.Roots.Where(p => p.root_reportList.Any(pr =>
                                    pr.shredder_dataList.Any(prr =>
                                        prr.shredder_data_descriptionList.Any(prrr =>
                                            prrr.document_id == description.document_id))))
                                .Include(_dbContext.GetIncludePaths(typeof(root))).FirstOrDefault();
                            if (dbResult != null)
                            {
                                var targetData = dbResult.root_reportList.First(pr => pr.shredder_dataList.Any(prr =>
                                    prr.shredder_data_descriptionList.Any(prrr =>
                                        prrr.document_id == description.document_id)));
                                report.CopyTo(targetData, "Id", true);
                                _dbContext.Update(dbResult);
                                result.root_reportList.RemoveAt(index);
                                index--;
                            }
                        }
                    }
                }

                if (result.root_reportList.Any())
                {
                    await _dbContext.Roots.AddAsync(result);
                }

                await _dbContext.SaveChangesAsync();
                return new OkObjectResult(new SuccessModel());
            }
            catch (Exception e)
            {
                return new OkObjectResult(new ErrorModel(e.Message));
            }
        }
    }
}