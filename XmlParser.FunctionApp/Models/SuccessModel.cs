﻿namespace XmlParser.FunctionApp.Models
{
    public class SuccessDataModel : SuccessModel
    {
        public object Data { get; set; }
        public SuccessDataModel(object data) : base()
        {
            Data = data;
        }
    }
    public class SuccessModel
    {
        public SuccessModel()
        {
            Success = true;
            Message = "Process performed Successfully";
        }
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class ErrorModel : SuccessModel
    {
        public string Exception { get; set; }
        public ErrorModel()
        {
            Success = false;
            Message = "Process performed UnSuccessfully";
        }

        public ErrorModel(string exception)
        {
            Success = false;
            Message = "Request performed UnSuccessfully";
            Exception = exception;
        }
    }
}