﻿namespace XmlParser.Actions
{
    public enum MainViewAction
    {
        Migrate,
        SelectFile,
        ReadFile,
        Clear,
        DeleteDatabase,
        GenerateClass
    }
}