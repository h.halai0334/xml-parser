﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using XmlParser.Domain;

namespace XmlParser.Context
{
    /// <summary>
    /// Db Context which allows use to perform database Operations
    /// </summary>
    public class ApplicationDbContext : DbContext
    {

        private string _connectionString;

        public ApplicationDbContext()
        {
            _connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        }

        public bool CreateAllTables()
        {
            var script =  Database.GenerateCreateScript();
            var regexScript = @"(?:\s|\r?\n)+GO(?:\s|\r?\n)+";
            script = Regex.Replace(script, regexScript,"");
            Database.ExecuteSqlRaw(script);
            return true;
        }
        public bool DeleteAllTables()
        {
            int count = 0;
            while (true)
            {
                try
                {
                    Database.ExecuteSqlRaw("EXEC sp_msforeachtable 'drop table ?';");
                    break;
                }
                catch (Exception e)
                {
                    count++;
                    if (count == 10)
                    {
                        break;
                    }
                }
            }
            return count != 10;
        }
        public ApplicationDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Configs
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<root> Roots { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }

    }
}