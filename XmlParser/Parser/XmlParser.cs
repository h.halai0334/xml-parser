﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.EntityFrameworkCore.Internal;
using XmlParser.Domain;
using XmlParser.XmlModels;

namespace XmlParser.Parser
{
    public class XmlParser
    {
        public static (Dictionary<string, HashSet<string>>, string) GenerateClassFromXml(string xml)
        {
            // Create Dictionary with Hashset for unique classes and properties
            Dictionary<string, HashSet<string>> dictionary = new Dictionary<string, HashSet<string>>();
            XmlDocument xmlDoc = new XmlDocument();
            // Load XML
            xmlDoc.LoadXml(xml);
            foreach (XmlNode childrenNode in xmlDoc.ChildNodes)
            {
                // Traverse All XML Elements
                if (childrenNode is XmlElement element)
                {
                    TraverseXml(dictionary, element);
                }
            }

            return (dictionary, "");
        }

        private static void TraverseXml(Dictionary<string, HashSet<string>> dictionary, XmlElement xmlElement, string className = "")
        {
            var nodes = xmlElement.ChildNodes;
            bool hasNodes = false;
            // Check if Node has Text Only or has Children
            if (nodes.Count > 0)
            {
                hasNodes = true;
                IEnumerator enumerator = nodes.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    object item = enumerator.Current;
                    if (item is XmlText)
                    {
                        hasNodes = false;
                    }
                    else
                    {
                        hasNodes = true;
                        break;
                    }
                }
            }
            // If Has Children
            if (hasNodes)
            {
                string name = "";
                
                // Check if it has "name" Attribute then Set it to name
                if (xmlElement.HasAttribute("name"))
                {
                    name = xmlElement.GetAttribute("name");
                }
                else
                {
                    // Else set element name
                    name = xmlElement.Name;
                }

                // Remove Spaces and capital letters
                name = name.Replace(" ", "_").ToLower();
                // If there is Class name add to the name
                if (!string.IsNullOrWhiteSpace(className))
                {
                    name = className + "_" + name;
                    // Remove "root_report_" from All Class names
                    name = name.Replace("root_report_", "");
                }
                // If Class is New then Add it in Dictionary
                if (!dictionary.ContainsKey(name))
                {
                    dictionary.Add(name,new HashSet<string>());
                }

                if (!string.IsNullOrWhiteSpace(className))
                {
                    // Get Class
                    var classValue = dictionary.GetValueOrDefault(className);
                    // Get Add Property as List
                    var property = "public List<" + name + "> " + name + "List {get; set;}";
                    if (classValue != null)
                    {
                        // Check if Property Exists
                        if (!classValue.Contains(property))
                        {
                            classValue.Add(property);
                        }    
                    }
                        
                }
                

                // Traverse All Child Nodes
                foreach (var childNode in xmlElement.ChildNodes)
                {
                    if (childNode is XmlElement node)
                    {
                        TraverseXml(dictionary,node, name);
                    }
                }

            }
            else
            {
                // If has no Child Nodes then its an Attribute
                string name = "";
                // Check if it has name Attribute
                if (xmlElement.HasAttribute("name") && xmlElement.Name == "entry")
                {
                    name = xmlElement.GetAttribute("name");
                }
                else
                {
                    // Else use Element Name
                    name = xmlElement.Name;
                }

                // Removing Space and # and lowering case
                name = name.Replace(" ", "_").Replace("#","No").ToLower();
                if (!string.IsNullOrWhiteSpace(className))
                {
                    // Get Class name
                    var classValue = dictionary.GetValueOrDefault(className);
                    // Check if property name is id then add class name
                    if (name == "id")
                    {
                        name = className + "_id";
                    }
                    // Add Property as String
                    var property = "public string " + name + " {get; set;}";
                    if (classValue != null)
                    {
                        if (!classValue.Contains(property))
                        {
                            classValue.Add(property);
                        }    
                    }

                    // Check if Element has more attributes
                    if (xmlElement.HasAttributes && xmlElement.Name != "entry" && xmlElement.Name != "entries")
                    {
                        // Loop over all attributes and unique ones int the class as Property
                        var attributeEnumerator = xmlElement.Attributes.GetEnumerator();
                        while (attributeEnumerator.MoveNext())
                        {
                            var item = (XmlAttribute)attributeEnumerator.Current;
                            if (item != null)
                            {
                                var attributeName = item.Name.Replace(" ", "_");
                                property = "public string " + name + "_" + attributeName +" {get; set;}";
                                if (classValue != null)
                                {
                                    if (!classValue.Contains(property))
                                    {
                                        classValue.Add(property);
                                    }    
                                }    
                            }
                            
                        }
                    }
                        
                }
            }
            // return classData;
        }

        private static object TraverseXmlAndParseIntoObject(object root, XmlElement xmlElement, string className = "")
        {
            // Get chi
            var nodes = xmlElement.ChildNodes;
            bool hasNodes = false;
            // Check if Node has Text Only or has Children
            if (nodes.Count > 0)
            {
                hasNodes = true;
                IEnumerator enumerator = nodes.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    object item = enumerator.Current;
                    if (item is XmlText)
                    {
                        hasNodes = false;
                    }
                    else
                    {
                        hasNodes = true;
                        break;
                    }
                }
            }
            // If Has Children
            if (hasNodes)
            {
                string name = "";
                // Check if it has "name" Attribute then Set it to name
                if (xmlElement.HasAttribute("name"))
                {
                    name = xmlElement.GetAttribute("name");
                }
                else
                {
                    // Else set element name
                    name = xmlElement.Name;
                }

                // Remove Spaces and capital letters
                name = name.Replace(" ", "_").ToLower();
                if (!string.IsNullOrWhiteSpace(className))
                {
                    name = className + "_" + name;
                    name = name.Replace("root_report_", "");
                }
                var properties = root.GetType().GetProperties();
                // Find property by name
                var foundProperty = properties.FirstOrDefault(p => p.Name == name + "List");
                if (foundProperty != null)
                {
                    // Create an instance of the Property List
                    var propertyValue = foundProperty.GetValue(root);
                    if (propertyValue == null)
                    {
                        // List Instantiate using Reflection in C#
                        var listType = typeof(List<>);
                        var constructedListType = listType.MakeGenericType(foundProperty.PropertyType.GetGenericArguments().Single());
                        var instance = Activator.CreateInstance(constructedListType);
                        var itemInstance = Activator.CreateInstance(foundProperty.PropertyType.GetGenericArguments().Single());
                        if (instance != null)
                        {
                            ((IList) instance).Add(itemInstance);
                            foundProperty.SetValue(root, instance);
                            root = itemInstance;
                        }
                    }
                    else
                    {
                        // Object Add/Instantiate
                        var itemInstance = Activator.CreateInstance(foundProperty.PropertyType.GetGenericArguments().Single());
                        ((IList) propertyValue).Add(itemInstance);
                        root = itemInstance;
                    }
                }
                // Loop over child Elements
                foreach (var childNode in xmlElement.ChildNodes)
                {
                    if (childNode is XmlElement node)
                    {
                        TraverseXmlAndParseIntoObject(root, node, name);
                    }
                }

            }
            else
            {
                string name = "";
                // Check if it has "name" Attribute then Set it to name
                if (xmlElement.HasAttribute("name") && xmlElement.Name == "entry")
                {
                    name = xmlElement.GetAttribute("name");
                }
                else
                {
                    // Else set element name
                    name = xmlElement.Name;
                }
                // Removing Space and # and lowering case
                name = name.Replace(" ", "_").Replace("#","No").ToLower();
                if (name == "id")
                {
                    name = className + "_id";
                }
                // Get all properties
                var properties = root.GetType().GetProperties();
                var foundProperty = properties.FirstOrDefault(p => p.Name == name);
                // If property found Set Text
                if (foundProperty != null) 
                {
                    foundProperty.SetValue(root,xmlElement.InnerText);
                }
                // Check for extra attributes
                if (xmlElement.HasAttributes && xmlElement.Name != "entry" && xmlElement.Name != "entries")
                {
                    // Loop over attributes
                    var attributeEnumerator = xmlElement.Attributes.GetEnumerator();
                    while (attributeEnumerator.MoveNext())
                    {
                        var item = (XmlAttribute)attributeEnumerator.Current;
                        if (item != null)
                        {
                            var attributeName = item.Name.Replace(" ", "_");
                            var propertyName = name + "_" + attributeName;
                            // Set value of found property
                            foundProperty = properties.FirstOrDefault(p => p.Name == propertyName);
                            if (foundProperty != null) 
                            {
                                foundProperty.SetValue(root,item.Value);
                            }
                        }
                            
                    }
                }

            }
            // Return Root of Object
            return root;
        }

        public static (root, string) ParseXml(string xml)
        {
            root root = new root();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);
                foreach (XmlNode childrenNode in xmlDoc.ChildNodes)
                {
                    // Loop over all Nodes
                    if (childrenNode is XmlElement element)
                    {
                        root = (root) TraverseXmlAndParseIntoObject(root,element);
                    }
                }
                return (root, "");
            }
            catch (Exception e)
            {
                return (root, e.Message);
            }
        }
    }
}