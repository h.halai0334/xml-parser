﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlParser.XmlModels
{
    [XmlRoot(ElementName="product_name")]
	public class Product_name {
		[XmlAttribute(AttributeName="id")]
		public string Id { get; set; }
		[XmlAttribute(AttributeName="name")]
		public string Name { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName="author")]
	public class Author {
		[XmlElement(ElementName="product_name")]
		public Product_name Product_name { get; set; }
		[XmlElement(ElementName="product_version")]
		public string Product_version { get; set; }
		[XmlElement(ElementName="product_revision")]
		public string Product_revision { get; set; }
	}

	[XmlRoot(ElementName="key")]
	public class Key {
		[XmlAttribute(AttributeName="identifier")]
		public string Identifier { get; set; }
	}

	[XmlRoot(ElementName="log_entry")]
	public class Log_entry {
		[XmlElement(ElementName="author")]
		public Author Author { get; set; }
		[XmlElement(ElementName="date")]
		public string Date { get; set; }
		[XmlElement(ElementName="integrity")]
		public string Integrity { get; set; }
		[XmlElement(ElementName="key")]
		public Key Key { get; set; }
	}

	[XmlRoot(ElementName="document_log")]
	public class Document_log {
		[XmlElement(ElementName="log_entry")]
		public List<Log_entry> Log_entry { get; set; }
	}

	[XmlRoot(ElementName="entry")]
	public class Entry {
		[XmlAttribute(AttributeName="name")]
		public string Name { get; set; }
		[XmlAttribute(AttributeName="type")]
		public string Type { get; set; }
		[XmlText]
		public string Text { get; set; }
		[XmlElement(ElementName="entry")]
		public List<Entry> EntryXml { get; set; }
		[XmlElement(ElementName="entries")]
		public List<Entries> Entries { get; set; }
	}

	[XmlRoot(ElementName="entries")]
	public class Entries {
		[XmlElement(ElementName="entry")]
		public List<Entry> Entry { get; set; }
		[XmlAttribute(AttributeName="name")]
		public string Name { get; set; }
		[XmlElement(ElementName="entries")]
		public List<Entries> EntriesXml { get; set; }
	}

	[XmlRoot(ElementName="description")]
	public class Description {
		[XmlElement(ElementName="document_id")]
		public string Document_id { get; set; }
		[XmlElement(ElementName="document_log")]
		public Document_log Document_log { get; set; }
		[XmlElement(ElementName="entry")]
		public Entry Entry { get; set; }
	}

	[XmlRoot(ElementName="shredder_erasure_report")]
	public class Shredder_erasure_report {
		[XmlElement(ElementName="entries")]
		public List<Entries> Entries { get; set; }
		[XmlAttribute(AttributeName="report")]
		public string Report { get; set; }
	}

	[XmlRoot(ElementName="shredder_hardware_report")]
	public class Shredder_hardware_report {
		[XmlElement(ElementName="entries")]
		public List<Entries> Entries { get; set; }
	}

	[XmlRoot(ElementName="shredder_data")]
	public class Shredder_data {
		[XmlElement(ElementName="description")]
		public Description Description { get; set; }
		[XmlElement(ElementName="shredder_erasure_report")]
		public Shredder_erasure_report Shredder_erasure_report { get; set; }
		[XmlElement(ElementName="shredder_hardware_report")]
		public Shredder_hardware_report Shredder_hardware_report { get; set; }
	}

	[XmlRoot(ElementName="user_data")]
	public class User_data {
		[XmlElement(ElementName="entries")]
		public Entries Entries { get; set; }
	}

	[XmlRoot(ElementName="report")]
	public class Report {
		[XmlElement(ElementName="shredder_data")]
		public Shredder_data Shredder_data { get; set; }
		[XmlElement(ElementName="user_data")]
		public User_data User_data { get; set; }
	}

	[XmlRoot(ElementName="root")]
	public class Root {
		[XmlElement(ElementName="report")]
		public List<Report> Report { get; set; }
	}
}