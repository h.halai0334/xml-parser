﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using XmlParser.Actions;
using XmlParser.Context;

namespace XmlParser.ViewModels
{
    public class MainViewModel : BaseViewModel<MainViewAction>
    {
        #region Private Members

        private string _connectionString;
        private string _filePath;
        private string _logText;
        private ApplicationDbContext _applicationDbContext;
        #endregion

        #region Public Members

        public string LogText
        {
            get => _logText;
            set
            {
                _logText = value;
                OnPropertyChanged();
            }
        }

        public string ConnectionString
        {
            get => _connectionString;
            set
            {
                _connectionString = value;
                OnPropertyChanged();
            }
        }

        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged();
            }
        }

        #endregion


        #region Protected Methods

        protected override async Task PerformAction(MainViewAction action)
        {
            ShowLoading();
            if (action != MainViewAction.Clear)
            {
                Log("*********************************************", isShowDate: false);
                Log("Starting : " + action);
            }
            switch (action)
            {
                case MainViewAction.Migrate:
                    Migrate();
                    break;
                case MainViewAction.SelectFile:
                    SelectFile();
                    break;
                case MainViewAction.ReadFile:
                    await ReadFile();
                    break;
                case MainViewAction.GenerateClass:
                    await GenerateClass();
                    break;

                case MainViewAction.Clear:
                    break;
                case MainViewAction.DeleteDatabase:
                    DeleteDatabase();
                    break;
            }
            if (action != MainViewAction.Clear)
            {
                Log("Completed : " + action);
                Log("*********************************************", isShowDate: false);
            }

            HideLoading();

        }

        private async Task GenerateClass()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    MessageBox.Show("File Path is Empty");
                    return;
                }

                var xml = await File.ReadAllTextAsync(FilePath);
                var (result, exception) = Parser.XmlParser.GenerateClassFromXml(xml);
                if (!string.IsNullOrWhiteSpace(exception))
                {
                    Log(exception, LogLevel.Critical);
                }
                else
                {
                    string classData = "";
                    foreach (KeyValuePair<string,HashSet<string>> classItem in result)
                    {
                        classData += "public class " + classItem.Key + ": Base {\n";
                        foreach (var property in classItem.Value)
                        {
                            classData += property + "\n";
                        }
                        classData += "}\n";
                    }
                    Log(classData,LogLevel.Critical,false);
                }
            }
            catch (Exception e)
            {
                Log(e.Message, LogLevel.Critical);
            }
        }


        public override void Init()
        {
            base.Init();
            ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        }

        public void Log(string message, LogLevel level = LogLevel.Information, bool isShowDate = true)
        {
            if (isShowDate)
            {
                var stringLevel = level.ToString();
                LogText += ($"[{DateTime.Now:T}] {stringLevel}:  {message}\n");
            }
            else
            {
                LogText += ($"{message}\n");
            }
        }

        private async Task ReadFile()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    MessageBox.Show("File Path is Empty");
                    return;
                }

                var xml = await File.ReadAllTextAsync(FilePath);
                var (result, exception) = Parser.XmlParser.ParseXml(xml);
                if (!string.IsNullOrWhiteSpace(exception))
                {
                    Log(exception, LogLevel.Critical);
                }
                else
                {
                    _applicationDbContext = new ApplicationDbContext(ConnectionString);
                    await _applicationDbContext.AddRangeAsync(result);
                    await _applicationDbContext.SaveChangesAsync();
                    Log("Saved to Db Successfully");
                }
            }
            catch (Exception e)
            {
                Log(e.Message, LogLevel.Critical);
            }
        }

        private void SelectFile()
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "XML Files|*.xml",
                Title = "Please select an XML File",
                Multiselect = false
            };
            if (dialog.ShowDialog() == true)
            {
                FilePath = dialog.FileName;
            }
        }

        private void DeleteDatabase()
        {
            try
            {
                _applicationDbContext = new ApplicationDbContext(ConnectionString);
                if (_applicationDbContext.DeleteAllTables())
                {
                    Log("Db Deleted Successfully");
                }
            }
            catch (Exception e)
            {
                Log(e.Message,LogLevel.Critical);
            }
        }
        private void Migrate()
        {
            try
            {
                _applicationDbContext = new ApplicationDbContext(ConnectionString);
                if (_applicationDbContext.CreateAllTables())
                {
                    Log("Db Created Successfully");
                }
                else
                {
                    if (_applicationDbContext.CreateAllTables())
                    {
                        Log("Db Created Successfully");
                    }
                }
            }
            catch (Exception e)
            {
                Log(e.Message,LogLevel.Critical);
            }
        }

        #endregion
    }
}