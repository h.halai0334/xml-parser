﻿using System;
using System.Configuration;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Extensions.Logging;
using XmlParser.Actions;
using XmlParser.ViewModels;

namespace XmlParser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            // Referencing Data Context
            _viewModel = DataContext as MainViewModel;
            _viewModel?.Init();
            InitHandler();
        }

        /// <summary>
        /// Handling all global Exceptions and Logging them on Console
        /// </summary>
        private void InitHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += AppDomainOnUnhandledException;
            Application.Current.DispatcherUnhandledException += ApplicationOnDispatcherUnhandledException;
            Dispatcher.UnhandledException += DispatcherOnUnhandledException;
        }

        private void DispatcherOnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _viewModel.Log(e.Exception.Message, LogLevel.Critical);
        }

        private void ApplicationOnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _viewModel.Log(e.Exception.Message, LogLevel.Critical);
        }

        private void AppDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _viewModel.Log(e.ExceptionObject.ToString(), LogLevel.Critical);
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (ConfigurationManager.AppSettings["DeleteDbOnStart"].ToLower() == "true")
            {
                _viewModel.ActionCommand.Execute(MainViewAction.DeleteDatabase);
            }
            if (ConfigurationManager.AppSettings["CreateDbOnStart"].ToLower() == "true")
            {
                _viewModel.ActionCommand.Execute(MainViewAction.Migrate);
            }
        }
    }
}